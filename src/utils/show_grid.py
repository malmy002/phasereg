import numpy as np
from matplotlib import pylab as plt


def show_grid(phi, n, domain_width=None, show_undeformed_grid=False, color="darkgrey",
              axis='off', aspect='equal', **kwargs):
    """
    Show the deformed grid
    :param phi: new positions: phi(x)
    :param n: number of lines
    :param aspect: aspect ratio of the figure
    :return:
    """
    L,M,N = phi.shape
    if domain_width is None:
        domain_width = [M,N]
    gridx, gridy = np.meshgrid(np.linspace(-1 / 2, 1 / 2, n),
                               np.linspace(-1 / 2, 1 / 2, n), indexing='ij')
    phi2 = phi(np.stack((gridx, gridy), axis=0))

    ax = plt.gca()
    if show_undeformed_grid:
        _plot_grid(ax, gridx, gridy, color=color, **kwargs)
    else:
        _plot_grid(ax, domain_width[0]*phi2[0, :, :] + M//2, 
                    domain_width[1]*phi2[1, :, :] + N//2, color=color, **kwargs)
    if axis == 'off':
        ax.set_axis_off()
    ax.set_aspect(aspect)


def _plot_grid(ax, gridx, gridy, **kwargs):
    for i in range(gridx.shape[0]):
        ax.plot(gridy[i,:], gridx[i,:], **kwargs)
    for i in range(gridx.shape[1]):
        ax.plot(gridy[:,i], gridx[:,i], **kwargs)







