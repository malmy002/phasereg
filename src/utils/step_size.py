import numpy as np


class StepSize(object):
    def __init__(self, N, approach="initial", vmax=None, it_stop=5,
        update_freq=100, ep_rate=0.1):
        """
        approach: 'initial', 'constant'
        it_stop: iteration number to stop updating dt ('initial' approach)
        update_freq: frequency * step number
        ep_rate: how quickly epsilon is updated (smaller=slower)
        """

        #Number of pixels along one direction
        self.N = N

        #Type of approach to take
        self.approach = approach

        #When to stop averaging for 'initial' approach
        self.it_stop = it_stop

        #List of max Vn values
        self.mx = []

        #Keep track of first 5 dts
        self.eps = []

        #Keep track of iterations
        self.it = 0

        # step size
        self.ep = 0.0

        #How often to update step size
        self.update_freq = update_freq

        #How quickly ep is updated in "custom"
        self.ep_rate = ep_rate

        #Maximum v value (1/500): Domain = [-0.5, 0.5] x [-0.5, 0.5]
        if vmax is None:
            self.vmax = 1.0 / (500.0)
        else:
            self.vmax = vmax

        #Keep track of previous vmax values
        self.vmax_values = []


    def __mul__(self, v):
        if self.approach in 'constant':
            self.constant(v)
        elif self.approach in 'initial':
            self.initial(v)
        elif self.approach in 'custom':
            self.custom(v)
        self.it += 1
        return self.ep * v


    def constant(self, v):
        """
        Keep max(Vn) = c (constant).
        """
        #current ||v||_infty
        max_v = np.linalg.norm(v, axis=0).max()
        if self.it==0:
            self.ep = float(self.vmax / max_v)
        self.vmax_values.append(max_v)
        if np.mod(self.it, self.update_freq) == 0:
            self.ep = float(self.vmax / np.max(self.vmax_values))
            self.vmax_values = []


    def initial(self, v):
        """
        Based on the first few velocities.
        """
        if self.it < self.it_stop:
            self.mx.append(np.abs(v).max())
            mn = np.mean(self.mx)
            self.ep = self.vmax / self.mx[-1]
        elif self.it == self.it_stop:
            mn = np.mean(self.mx)
            self.ep = self.vmax/mn


    def custom(self, v):
        """
        Keep max(Vn) = c (constant).

        Updates ep slower than "constant" -> more stable.
        """
        #current ||v||_infty
        max_v = np.linalg.norm(v, axis=0).max()
        if self.it==0:
            self.ep = float(self.vmax / max_v)
        self.vmax_values.append(max_v)
        if np.mod(self.it, self.update_freq) == 0:
            ep = float(self.vmax / np.max(self.vmax_values))
            self.ep = (1-self.ep_rate)*self.ep + self.ep_rate*ep
            self.vmax_values = []
