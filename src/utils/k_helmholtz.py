"""
K operator for solving the Poisson equation using fft method.

K := F o P_s o F^{-1}

P_s: support projection
F: Fourier transform

"""
import numpy as np
from numpy.fft import fft2, ifft2, fftshift
from ..forms import Form


class KHelmholtz(object):
    def __init__(self, alpha, gamma):
        """
        """
        self.alpha = alpha
        self.gamma = gamma
        self.shape = None

    def __call__(self, v):
        """
        Operate on n-form.
        """
        if self.shape is None:
            self.shape = np.array([v.shape[-2],v.shape[-1]])
            self.dx = 1.0/self.shape[-1]
            self.v2 = Form(1, shape=v.shape)
            self._init()

        vf = fft2(fftshift(v, axes=(1,2)), axes=(1,2))
        self.v2[:] = np.real(fftshift(ifft2(np.einsum('ijk,jk->ijk',vf,self.B),
                                                 axes=(1,2)), axes=(1,2)))
        return self.v2


    def _init(self):
        """
        """
        k00 = np.arange(self.shape[-2]) - self.shape[-2]/2
        k0,k1 = np.array(np.meshgrid(k00,k00))
        self.A = fftshift(self.gamma + 2.0 * self.alpha * \
                     ((1.0 - np.cos(2 * np.pi * self.dx * k0)) / np.square(self.dx) +
                        (1.0 - np.cos(2 * np.pi * self.dx * k1)) / np.square(self.dx)))

        self.B = 1.0/np.square(self.A)
        self.small = self.A<1e-5
