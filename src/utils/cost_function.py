import numpy as np
from numpy.fft import fft2, fftshift, ifft2
from scipy.linalg import norm


def cost1(vs, K, sigma):
    """
    Calculate the cost from the distance from the identity transformation.
    d(phi, id)^2

    vs: list of velocity fields (njord 1-form)
    K: K Helmholtz operator
    sigma: positive constant
    """
    l = len(vs)
    cost = 0
    for a in range(l):
        cost += vel_norm_squared(vs[a], K)
    cost = sigma * cost/l
    return cost


def vel_norm_squared(v, K):
    """
    Calculate velocity field norm in V.
    ||v||^2_V

    v: velocity field njord 1-form
    K: K Helmholtz
    """

    if not hasattr(K, 'A'):
        temp = K(v)
    vf = fft2(fftshift(v, axes=(1, 2)), axes=(1, 2))
    v2 = np.real(fftshift(ifft2(np.einsum('ijk,jk->ijk', vf, K.A),
                                        axes=(1, 2)), axes=(1, 2)))
    norm_squared = K.dx**2 * norm(v2.flat, ord=2)**2
    return norm_squared
