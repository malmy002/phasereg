"""
Process volume forms for LDDMM CDI.
"""
import numpy as np
from numpy.fft import fft2, ifft2, fftshift
from njord_np import Form


def scale_volforms(f0, g, bg=0, use_g_center=False):
    """
    Images: centered

    Scale volume forms for CDI.

    Normalizes images and sets f0 based on total mass.

    f0: initial guess
    f1: amplitude data in Fourier space
    bg: background value
    return: f0, g
    """

    def change_range(f, bg=1e-4):
        f -= f.min()
        f /= f.max()
        f *= (f.max() - bg)
        f += bg
        return f

    #Normalize g
    g /= g.max()

    #change I0 to ensure total masses are equal.
    f0 = change_range(f0, bg=bg)

    if use_g_center:
        gsum = np.sqrt(g.size) * np.real(fftshift(g))[0,0]
    else:
        gsum = np.sum(np.real(ifft2(fftshift(g), norm='ortho')))

    f0sum = float(np.sum(f0 - bg))
    f0 = gsum * f0 / float(f0sum)
    I0 = Form(2, array=f0.astype('float'))
    I1 = Form(2, array=g.astype('float'))
    return I0, I1
