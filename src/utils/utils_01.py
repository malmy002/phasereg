"""
Utility functions for LDDMM CDI.
"""
import numpy as np
from numpy.fft import fft2, ifft2, fftshift


def auto_sup(g, thresh=0.15):
    """
    Determine autocorrelation support.
    :param g: amplitude data
    :param thresh: threshold
    :return: autocorrelation support
    """
    auto = np.sqrt(np.maximum(np.real(ifft2(np.abs(g) ** 2, norm='ortho')),0.0))
    return np.abs(auto) > np.abs(auto).max() * thresh

def total_mass(g):
    """
    Calculate the total mass from the far-field amplitude data.
    :param g: amplitude data (for CDI)
    :return: total mass
    """
    return np.sum(np.real(ifft2(g, norm='ortho')))


def geo_factor(asup, sup):
    """
    Estimate geometric factor: |A|/|S|
    Autocorrelation support size/ support size.
    :param asup: autocorrelation support
    :param sup: support
    :return: G=|A|/|S|
    """
    return asup.sum() / sup.sum()

def support(f, thresh=0.3):
    """
    Determine support of image (f)
    :param f: image
    :param thresh: threshold
    :return: support
    """
    return f > thresh * np.abs(f).max()


def auto_sup_image(f, thresh=0.15):
    """
    Determine autocorrelation support for image: f.
    :param f: image
    :param thresh: threshold
    :return: autocorrelation support
    """
    g = np.abs(fft2(f, norm='ortho'))
    auto = np.sqrt(np.abs(np.real(ifft2(np.abs(g) ** 2, norm='ortho'))))
    return auto > auto.max() * thresh


def geometry_factor(img, thresh1=0.1, thresh2=0.05):
    """
    Estimate geometry factor: |A|/|S| from image.
    :param thresh:
    :param img:
    :return:
    """
    sup = support(img, thresh=thresh1)
    Ssize = np.sum(sup)
    auto_sup = auto_sup_image(img, thresh=thresh2)
    Asize = np.sum(auto_sup)
    return Asize/Ssize

