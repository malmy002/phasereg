import numpy as np
from numpy.fft import fft2, ifft2, fftshift
from njord_np import Form
from .utils_01 import support

def normalize_0form_cdi(I0, I1, thresh=0.10, geom_factor=4.0):
    """
    ----------------
    Images: centered
    -----------------

    Normalize I0 to data (I1) for CDI.
    :param I0: Initial guess (real-space)
    :param I1: Fourier data (amplitudes) centered
    :param thresh: threshold
    :param geom_factor: geometry factor (between 2 - 3) 2: rectangle, 3:triangle
    :return: I0 renormalized
    """
    auto = np.real(ifft2(fftshift(I1 ** 2), norm='ortho'))
    M = np.sum(np.maximum(np.real(ifft2(fftshift(I1), norm='ortho')), 0.0))
    sqrt_auto = np.sqrt(np.maximum(np.real(ifft2(fftshift(I1**2), norm='ortho')), 0.0))
    auto_sup = sqrt_auto > thresh * sqrt_auto.max()
    amp = geom_factor * M / (auto_sup.sum() )
    I0[:] = amp * I0 / I0.max()
    I0 = Form(0, array=I0)
    return I0
