"""
K operator

K := F o P_s o F^{-1}

P_s: support projection
F: Fourier transform

"""
import numpy as np
from scipy.ndimage.filters import gaussian_filter


class K(object):
    def __init__(self, sig=6.0, mode='constant', cval=0):
        """
        K for gaussian filtering
        :param sig:
        :param mode: what type of mode for boundaries (constant:lddmm, wrap:mass_preserv. lddmm)
        :param cval: fillin values
        """
        self.sig = sig
        self.mode = mode
        self.cval = cval

    def __call__(self, v):
        """
        Operate on 1-form
        """

        for a in range(2):
            v[a, :, :] = gaussian_filter(v[a, :, :], self.sig, mode=self.mode,
                                         cval=self.cval)
        return v

        


    
