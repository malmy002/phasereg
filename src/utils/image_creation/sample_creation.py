import numpy as np
import scipy as sci


def rectangle(shape, widths=[10,10], smooth=0, angle=0, shift=[0,0]):
    """ Rectangular image
    shape: image shape, tuple
    widths: rectangle widths
    smooth: standard deviation of gaussian smooth
    angle: rotation angle
    """
    M,N = shape
    w1,w2 = widths
    img = np.zeros(shape)
    img[M//2-w1:M//2+w1, N//2-w2:N//2+w2] = 1.0
    img = sci.ndimage.gaussian_filter(img, sigma=smooth)
    img = sci.ndimage.rotate(img, angle, reshape=False)
    img = sci.ndimage.shift(img, np.round(shift), order=0, mode="wrap")
    return img

    

