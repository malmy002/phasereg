"""
Gradient operator.
"""
import numpy as np
from numpy.fft import fft2, fftshift, ifft2
from numpy import real, stack, array

def grad(f):
    """ Gradient of a function (0-form).
    f: src.Form(0)
    """
    return np.array(np.gradient(f, f.dx[0], f.dx[1], axis=(-2,-1), edge_order=2))

def pgrad(f):
    """ Periodic gradient of a function (0-form).
    f: src.Form(0)
    """
    ff = fft2(fftshift(f), norm='ortho')
    x0,x1 = 2*(f.shape[0])*np.pi*fftshift(f.x, axes=(1,2))
    fgrad = stack((1j * x0 * ff,
                     1j * x1 * ff), axis=0)
    return array(fftshift(real(ifft2(fgrad, axes=(1,2), norm='ortho')),axes=(1,2)))
