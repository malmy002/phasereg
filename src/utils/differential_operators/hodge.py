"""
Hodge operator
"""
from phasereg import Form
import numpy as np

def hodge(form, phi=None, phi_inv=None):
    """ Hodge operator in curved space.
    Natural operator.

    m = g.pull(phi)
    g: flat metric
    phi: forward phi (Form)
    phi_inv: inverse phi (Form)
    """
    if phi is None and phi_inv is None:
        return hodge_flat(form)
    elif phi is not None and phi_inv is not None:
        return(hodge_flat(form.pull(phi_inv)).pull(phi))

def hodge_flat(form):
    """ Hodge operator in E^2.
    """
    if form.n == 0:
        return Form(2, array=form)
    elif form.n == 1:
        return Form(1, array= np.einsum('i,ijk->ijk', 
            np.array([-1.0,1.0]), np.flip(form, axis=0)))
    elif form.n == 2:
        return Form(0, array=form)
