"""
The exterior derivative adjoint operator ($\delta$).

\delta operator:
"""
from . import div, grad, curl
from phasereg import Form

def delta(form, phi=None, phi_inv=None):
    """ Codifferential: ~ Adjoint exterior derivative (without boundary term).
    form: n-form, class: Form
    delta_m(form) = phi^*(delta_g(phi_inv^* form) )
    m = g.pull(phi)
    g: flat metric
    phi: forward phi (Form)
    phi_inv: inverse phi (Form)
    delta_g: flat codifferential
    """

    # Flat or curve space codifferential
    if phi is None or phi_inv is None:
        return delta_flat(form)
    elif phi is not None and phi_inv is not None:
        return(delta_flat(form.pull(phi_inv)).pull(phi))
        

def delta_flat(form):
    """ Flat Codifferential: ~ Adjoint exterior derivative 
    (without boundary term).

    form: n-form
    """
    if form.n == 0:
        return 0.0 * form
    elif form.n == 1:
        return Form(0, array=div(form))
    elif form.n == 2:
        return Form(1, array=-curl(form))
