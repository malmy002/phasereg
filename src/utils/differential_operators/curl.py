"""
Used for d in 2D.
"""
import numpy as np


def curl(v):
    """ Curl of vector-field (in 2D).
    v.shape: [2,M,N]
    """
    jac = np.array(np.gradient(v, v.dx[0], v.dx[1], axis=(1,2)))  #[2,2,M,N]
    curl = -(jac[0,1,:,:] - jac[1,0,:,:])
    return curl
