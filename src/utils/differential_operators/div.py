"""
Divergence operator.
"""
from .grad import grad
import numpy as np


def div(v):
    """Divergence of vector field
    v.shape: [2,M,N]
    """
    jac = np.array(np.gradient(v, v.dx[0], v.dx[1], axis=(1,2)))  #[2,2,M,N]
    return jac[0,0,:,:] + jac[1,1,:,:]
