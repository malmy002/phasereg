"""
Exterior derivative.
"""
from . import grad, div, curl
from phasereg import Form

def d(form):
    """ Exterior derivative
    """
    if form.n==0:
        return Form(1, array=grad(form))
    elif form.n ==1:
        return Form(2, array=curl(form))
    elif form.n == 0:
        return 0.0*form
