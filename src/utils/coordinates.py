import numpy as np

def make_coordinates(dx):
    """
    Make coordinates over [0,1] with spacing dx.

    return: (2,M,N) positions
    """
    x0 = np.arange(0,1,dx[0]) - 0.5
    x1 = np.arange(0,1,dx[1]) - 0.5
    x0m,x1m = np.meshgrid(x0, x1, indexing='ij')
    return np.stack([x0m,x1m], axis=0)

