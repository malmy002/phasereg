import numpy as np
from njord_np import Form


def inverse(phi, phi_inv=None, step_size=1e-1, max_iter=500):
    """
    Calculate the inverse of phi.
    See: Avants 2008 "Symmetric diffeomorphic image registration with..."
    (algorithm 2)
    """
    N = phi.shape[-1]

    #inverse guess
    if phi_inv is None:
        psi = Form(1, array=phi.x)
    else:
        psi = Form(1, array=phi_inv.copy())

    vinv = Form(1, array=(psi(phi) - phi.x).copy())

    it = 0
    accuracy = 0.5/N
    while (np.max(np.linalg.norm(psi(phi) - phi.x, axis=0)) > accuracy) and it < max_iter:
        vinv[:] = (psi(phi) - phi.x)[:]
        gamma = (step_size/N)/np.max(np.linalg.norm(vinv, axis=0))
        psi[:] -= gamma * vinv(psi)[:]
        it += 1
    print(it, np.max(np.linalg.norm(psi(phi) - phi.x, axis=0)))
    return psi
