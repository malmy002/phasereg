import numpy as np
from numpy.fft import fft2, fftshift, ifft2
from scipy.linalg import norm


def distance(vs, K):
    """
    Calculate the distance metric for a list of vector fields (1-forms).

    See: LDDMM paper.

    :param vs: velocity fields (list)
    :param K: K operator (see: k_helmholtz)
    :return: distance (dist)
    """
    #number of velocity fields (pseudo time steps)
    n = len(vs)

    norms = []
    for a, v  in enumerate(vs):
        vf = fft2(fftshift(v, axes=(1, 2)), axes=(1, 2))
        v2 = np.real(fftshift(ifft2(np.einsum('ijk,jk->ijk', vf, K.A),
                                            axes=(1, 2)), axes=(1, 2)))
        norms.append(K.dx**2 * norm(v2.flat, ord=2)**2)
    norms = np.sqrt(np.array(norms))
    dist = (1.0/n)*np.sum(norms)
    """Should actually be:"""
    # norms = np.array(norms)
    # dist = (1.0/n)*np.sqrt(np.sum(norms))
    return dist
