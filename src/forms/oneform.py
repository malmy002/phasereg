"""

Class for 1-forms.

Domain: [-1/2,1/2]x[-1/2,1/2]
"""

import numpy as np
from scipy.interpolate import RectBivariateSpline
from .baseform import BaseForm
from .zeroform import ZeroForm




class OneForm(BaseForm):

    # 1-form (n=1)
    n=1

    def _interp1(self):
        x = np.linspace(0, 1, self.shape[1]) - 0.5
        y = np.linspace(0, 1, self.shape[-1]) - 0.5
        self.interpolator1 =  RectBivariateSpline(x, y, self[0,:,:])
        self.interpolator2 =  RectBivariateSpline(x, y, self[1,:,:])

    def __call__(self, x):
        """
        x: shape: [2, M, N]
        x = (y,x) - matrix indexing
        """
        if ~hasattr(self, 'interpolator'):
            self._interp1()
        # x = -0.5 + (x + 0.5) % 1
        f1 = self.interpolator1.ev(x[0,:,:], x[1,:,:])
        f2 = self.interpolator2.ev(x[0,:,:], x[1,:,:])
        ftot = np.stack((f1,f2), axis=0)
        return self.__class__(array=ftot)

    def push(self, phi_inv, phi=None):
        """ Push-forward
        """
        if phi is None:
            return self.pull(phi_inv)
        else:
            return self.pull(self._phi_inv(phi))

    def pull(self, phi):
        """ Pull-back
        """
        return self._mul(self(phi), self._jac(phi))


    def _jac(self, phi):
        """
        """
        g = np.gradient(phi, self.dx[0], self.dx[1], axis=[1,2])
        M = np.array([[g[0][0,:,:], g[1][0,:,:]], [g[0][1,:,:], g[1][1,:,:]]])
        return M

    def _mul(self, v, A):
        return self.__class__(array=np.einsum('ijk,ihjk->hjk',v,A))

    def _phi_inv(self, phi):
        """
        For calculating phi inverse.
        """
        phi_inv = self.__class__(array=2.0*self.x - phi)
        return (phi_inv)

    def jac_det(self, phi):
        """
        Jacobian determinant |D phi|
        """
        g = np.gradient(phi, self.dx[0], self.dx[1], axis=(1,2))
        return ZeroForm(array=np.abs(g[0][0,:,:] * g[1][1,:,:] - g[1][0,:,:] * g[0][1,:,:]))
