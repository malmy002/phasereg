"""

Base class for n-forms.


Domain: [0,1]x[0,1]

"""
import numpy as np
from scipy import interpolate
from scipy.interpolate import RectBivariateSpline
from .zeroform import ZeroForm
from .oneform import OneForm
from .twoform import TwoForm
from .vectorfield import VField



class Form(np.ndarray):
    def __new__(subtype, n, array=None, shape=None, dtype=float,
                buffer=None, offset=0,
                strides=None, order=None, info=None):
        """
        n: n-form {-1,0,1,2} (-1 for vector field)
        """
        if array is not None:
            shape = array.shape
            if shape[0] !=2 and (n==1 or n==-1):
                print('Array is the wrong size for this n-form.')
        elif shape is not None:
            if len(shape) != 3:
                shape = [n, shape[0], shape[1]]
        else:
            print('Need either an array or shape')

        if n==0:
            cls = ZeroForm
        elif n==1:
            cls = OneForm
        elif n==2:
            cls = TwoForm
        elif n==-1:
            cls = VField

        obj = cls(array=array, shape=shape, dtype=dtype,
                                    buffer=buffer, offset=offset,
                                    strides=strides,order=order,info=info)
        if array is not None:
            obj[:] = array[:]
        else:
            obj[:] = np.zeros((shape))[:]

        obj.info = info
        obj.n = n # Type of form
        return obj




