"""

Class for 1-forms.

"""

from .baseform import BaseForm


class ZeroForm(BaseForm):

    #0-form
    n=0

    def pull(self, phi):
        """ Pull-back
        Override Form
        """
        return self(phi)
