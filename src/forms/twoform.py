"""

Class for 1-forms.

"""

import numpy as np
from scipy.interpolate import RectBivariateSpline
from .baseform import BaseForm
from .vectorfield import VField


class TwoForm(BaseForm):

    # Two-form (n=2)
    n = 2

    def push(self, phi_inv, phi=None):
        """ Push-forward
        Override Form
        """
        if phi is None:
            return self.pull(phi_inv)
        else:
            return self.pull(self._phi_inv(phi))

    def pull(self, phi):
        """ Pull-back
        Override Form
        """
        return self.__class__(array=(self._jac_det(phi) * self(phi)))

    def _jac_det(self, phi):
        """
        Jacobian determinant |D phi|
        """
        g = np.gradient(phi, self.dx[0], self.dx[1], axis=(1,2))
        return np.abs(g[0][0,:,:] * g[1][1,:,:] - g[1][0,:,:] * g[0][1,:,:])

    def _phi_inv(self, phi):
        """
        For calculating phi inverse.
        """
        phi_inv = VField(array=2.0*self.x - phi)
        return phi_inv
