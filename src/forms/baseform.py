"""

Base class for n-forms.


Domain: [-1/2,1/2]x[-1/2,1/2]

Coordinates: row, column (i,j)

"""
import numpy as np
from scipy import interpolate
from scipy.interpolate import RectBivariateSpline


class BaseForm(np.ndarray):
    """ Coordinates: (row, column) (i,j)
    """
    def __new__(subtype, array=None, shape=None, dtype=float, buffer=None, offset=0,
                strides=None, order=None, info=None):

        if array is not None:
            shape = array.shape

        obj = super(BaseForm, subtype).__new__(subtype, shape, dtype,
                                                buffer, offset, strides,
                                                order)
        if array is not None:
            obj[:] = array[:]
        else:
            obj[:] = np.zeros(shape)[:]

        obj.info = info

        x0 = np.linspace(0, 1, obj.shape[-2]) - 0.5
        x1 = np.linspace(0, 1, obj.shape[-1]) - 0.5
        x0m,x1m = np.meshgrid(x0,x1,indexing='ij')
        obj.x = np.stack((x0m, x1m), axis=0)
        obj.dx = np.array([1.0/(obj.shape[-2]-1), 1.0/(obj.shape[-1]-1)])
        obj.min_value = np.min(obj)
        return obj

    def __array_finalize__(self, obj):
        if obj is None: return
        self.info = getattr(obj, 'info', None)
        self.x = getattr(obj, 'x', None)
        self.dx = getattr(obj, 'dx', None)
        self.min_value = getattr(obj, 'min_value', None)

    def _interp1(self):
        x0 = np.linspace(0, 1, self.shape[-2]) - 0.5
        x1 = np.linspace(0, 1, self.shape[-1]) - 0.5
        x0m,x1m = np.meshgrid(x0,x1,indexing='ij')
        self.x0 = x0.copy()
        self.interpolator = RectBivariateSpline(x0, x1, self)#, bbox=[-0.5, 0.5, -0.5, 0.5])


    def __call__(self, x):
        """
        x: shape: [2, M, N]
        x = (y,x) - matrix indexing
        """
        """Rectbivariate spline"""
        if ~hasattr(self, 'interpolator'):
            self._interp1()
        if np.isscalar(x):
            x = x * self.x
        else:
            x = -0.5 + (x + 0.5) % 1
        return self.__class__(array=self.interpolator.ev(x[0,:,:], x[1,:,:]))
