"""

Class for 1-forms.

"""

import numpy as np
from scipy.interpolate import RectBivariateSpline
from .baseform import BaseForm



class VField(BaseForm):

    def _interp1(self):
        x = np.linspace(0, 1, self.shape[1])
        y = np.linspace(0, 1, self.shape[-1])
        self.interpolator1 =  RectBivariateSpline(x, y, self[0,:,:])
        self.interpolator2 =  RectBivariateSpline(x, y, self[1,:,:])
        
    def __call__(self, x):
        """
        x: shape: [2, M, N]
        x = (y,x) - matrix indexing
        """
        if ~hasattr(self, 'interpolator'):
            self._interp1()
        f1 = self.interpolator1.ev(x[0,:,:], x[1,:,:])
        f2 = self.interpolator2.ev(x[0,:,:], x[1,:,:])
        ftot = np.stack((f1,f2), axis=0)
        return self.__class__(array=ftot)
    
    def push(self, phi_inv, phi=None):
        """ Push-forward
        Override Form
        """
        if phi is None:
            a1 = self(phi_inv)
            phi = self._phi_inv(phi_inv)
            jac = self._jac(phi)
            return self._mul(jac(phi_inv), a1)
        else:
            phi = self._phi_inv(phi_inv)
            return self.pull(self._phi_inv(phi))

    def pull(self, phi):
        """ Pull-back
        Override Form
        """
        return self(phi)
        
        
    @property
    def jac(self):
        """ Jacobian of vector field
        """
        return self._jac(self)

    def _jac(self, phi):
        """
        """
        g = np.gradient(phi, self.dx[0], self.dx[1], axis=[1,2])
        M = np.array([[g[0][0,:,:], g[1][0,:,:]], [g[0][1,:,:], g[1][1,:,:]]])
        return M

    def _mul(self, A, v):
        v1 = v[0,:,:] * A[0,0,:,:] + v[1,:,:] * A[0,1,:,:]
        v2 = v[0,:,:] * A[1,0,:,:] + v[1,:,:] * A[1,1,:,:]
        return self.__class__(array=np.stack((v1,v2), axis=0))
        
