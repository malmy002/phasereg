"""
Gradient descent solver for CDI.
"""
import numpy as np
from phasereg import Form, K, grad, pgrad, KHelmholtz, TwoForm, ZeroForm
from phasereg.src.utils import scale_volforms, support, normalize_0form_cdi
from numpy.fft import fft2, ifft2, fftshift


class GradDescentCDI(object):
    def __init__(self, I0, I1, K, bg=0, eps=0, sigma=0,
                 save_best=0, cost='amplitude', check_error=True):
        """
        Class for solving image registration with gradient descent.

        :param I0: 2D numpy array
        :param I1: 2D numpy array
        :param K: K operator (RHKS)
        :param bg: background used for mass-preserving LDDMM
        """

        # Type
        self.type = "gradientcdi"

        self.eps = eps
        self.bg = bg
        self.I1norm = np.linalg.norm(I1.flat, ord=2)
        self.I1norm2 = np.linalg.norm((I1**2).flat, ord=2)

        # Initial images
        self._init_forms(I0, I1)

        M, N = self.I0.shape
        self.sh = self.I0.shape

        self.it = 0

        # Velocity field
        self.v = Form(1, shape=(2, M, N))

        # K operator
        self.K = K

        # Saving the best results
        self.save_best = save_best
        class BestResults: pass
        self.best_results = BestResults

        #Diffeomorphism
        self.phi = Form(1, array=self.v.x)

        #Approximate inverse
        self.phi_inv = Form(1, array=self.v.x)

        #velocity regularization coefficient
        self.sigma = sigma

        self.error = []

        # History
        self.J_hist = [] #saving Js
        self.it_hist = [] #saving iteration numbers
        self.phi_diff_hist = [] #displacement (phi - x)

        #Type of cost: amplitude or intensity (intensity is robust to noise)
        self.cost = cost.lower()

        self.check_error = check_error

    def _init_forms(self, I0, I1):
        """
        """
        if type(I0) is TwoForm:
            self.n = 2
        else:
            self.n = 0
        self.J = Form(self.n, array=np.array(I0))
        self.I0 = Form(self.n, array=I0)
        self.I1 = Form(self.n, array=fftshift(I1))
        self.R = Form(self.n, array=np.array(I0))

    def rescale(self, thresh=0.2):
        """
        Rescale I0 and J based on median value in support.
        :return:
        """
        if self.n == 0:
            sup = support(self.J, thresh=thresh)
            sc = np.median(self.J[sup])
            self.I0 *= sc/self.I0.max()
            self.J *= sc / self.J.max()

    def run(self, itnum, step_size=1e-3, save=None, K=None):
        """
        Run LDDMM
        """

        if K is not None:
            self.K = K

        for a in range(itnum):
            self.J = self.I0.pull(self.phi)
            Jf = fft2(self.J, norm='ortho')
            self.R0 = (np.abs(Jf) - self.I1)
            if self.cost in 'amplitude':
                R = np.exp(1j * np.angle(Jf)) * (np.abs(Jf) - self.I1)
            else:
                R = Jf * (np.abs(Jf) ** 2 - self.I1 ** 2)
            if self.eps:
                self.R[:] = np.real(fftshift(ifft2(R /
                                                   (1.0 + (self.eps / (self.I1 + 1e-1 * self.eps)) ** 2),
                                                   norm='ortho')))
            else:
                self.R[:] = np.real(ifft2(R, norm='ortho'))

            #Check type of form
            if self.n == 0:
                self.v += step_size *(-self.sigma*self.v - self.K(pgrad(self.J) * self.R))
                self.phi = self.v.x + self.v
                # self.phi -= step_size * self.K(pgrad(self.J) * self.R)
                self.phi_inv += step_size * self.K(pgrad(self.J) * self.R)
            elif self.n == 2:
                self.v += step_size * (-self.sigma * self.v + self.K(self.J * pgrad(self.R)))
                self.phi = self.v.x + self.v
                # self.phi += step_size * self.K(self.J * pgrad(self.R))
                self.phi_inv -= step_size * self.K(self.J * pgrad(self.R))

            # Calculate error
            self.calculate_error(self.it)

            # Save
            self.save(self.it, save)

            self.it += 1

            if (len(self.error) > 2) & self.check_error:
                if self.error[-1] > np.mean(self.error[-10:]):
                    self.revert_to_best()
                    self.error.pop(-1)
                    break


    def save(self,it, save):
        if save:
            if np.mod(it, save) == 0:
                self.save_results()

    def save_results(self):
        self.J_hist.append(self.J)
        self.it_hist.append(self.it)

    def calculate_error(self, it):
        if np.mod(it, 10) == 0:
            self.error.append(np.linalg.norm(np.abs(self.R0).flat, 2) / self.I1norm)

            if self.save_best:
                if self.error[-1] == np.min(self.error):
                    self.save_best_results()


    def save_best_results(self):
        """
        Save best results
        :return:
        """
        self.best_results.J = self.J.copy()
        self.best_results.phi = self.phi.copy()
        self.best_results.phi_inv = self.phi_inv.copy()
        self.best_results.R = self.R.copy()

    def revert_to_best(self):
        self.J = self.best_results.J.copy()
        self.phi = self.best_results.phi.copy()
        self.phi_inv = self.best_results.phi_inv.copy()
        self.R = self.best_results.R.copy()


    def _init_twoforms(self, I0, I1):
        """
        :param I0:
        :param I1:
        :return:
        """
        self.I0 = Form(2, array=I0)
        self.I1 = Form(2, array=fftshift(I1))



