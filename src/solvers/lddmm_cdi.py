"""

LDDMM solver

IMPORTANT NOTE: Update order is important: vs -> phis -> Js

"""
import numpy as np
from numpy.fft import fft2, ifft2, fftshift
from phasereg import Form, pgrad, grad, KHelmholtz
from .lddmm_base import LDDMMBase
from ..utils.cost_function import cost1


class LDDMM_CDI(LDDMMBase):
    def __init__(self, I0, I1, K, step_num=5, sigma=0, eps=0,
                 save_best=True, check_error=True):
        """
        Class for solving LDDMM for CDI.
        I0: 2D numpy array, template image (centered)
        I1: 2D numpy array, far-field amplitude data (centered)
        K: K operator (RHKS)
        step_num: number of time steps (~5)
        sigma: weight for regularization term (||v||)
        eps: regularization constant (in Fourier space)
        """

        LDDMMBase.__init__(self, I0, I1, K,
                           step_num=step_num, sigma=sigma, save_best=save_best,
                           check_error=check_error)

        # Type
        self.type = "lddmm_cdi"

        #Regularization constant
        self.eps = eps

        M, N = self.I0.shape

        #Saving results
        self.J_hist = []  # saving Js
        self.it_hist = []  # saving iteration numbers

        #Residual
        self.R0  = Form(0, array=np.zeros((M,N)))

        self.Js10 = []
        for a in range(step_num):
            self.Js10.append(Form(0,array=self.I0))

        self.update_Js()


    def save_results(self):
        self.J_hist.append(self.Js0[-1])
        self.it_hist.append(self.it)

    def calculate_error(self):
        self.error.append(np.linalg.norm(self.R0.flat, 2) / self.I1norm)
        self.cost1.append(cost1(self.vs, self.K, self.sigma))
        self.total_cost.append(self.error[-1] + self.cost1[-1])


    def update_velocities(self, step_size):
        """ Calculate velocities
        """

        for b in range(self.step_num):
            # self.vs[b] = self.vs[b] - step_size * (self.sigma * self.vs[b] -
            #     self.K(self.phis1[b].jac_det(self.phis1[b]) *
            #     pgrad(self.Js0[b]) * (self.Js0[b] - self.Js1[b])))

            self.vs[b] = self.vs[b] - step_size * (self.sigma * self.vs[b] -
                self.K(self.phis1[b].jac_det(self.phis1[b]) *
                pgrad(self.Js10[b]) * (self.Js10[b] - self.Js1[b])))


    def _init_forms(self,I0,I1):
        """
        """
        self.I0 = Form(0, array=I0)
        self.I1 = Form(0, array=fftshift(I1))


    def update_Js(self):
        """Update Js
        """
        uf = fft2(fftshift(self.Js0[-1]), norm='ortho')
        Jf = self.I1 * np.exp(1j * np.angle(uf))
        J1 = np.real(fftshift(ifft2(Jf, norm='ortho')))
        J1 = Form(0, array=J1)

        for b in range(self.step_num):
            self.Js10[b] = self.Js0[-1](self.phis1[b])

        for b in range(self.step_num):
            self.Js0[b] = self.I0(self.phis0[b])
            self.Js1[b] = J1(self.phis1[b])

        self.R0 = np.abs(uf) - self.I1
