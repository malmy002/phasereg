"""
LDDMM solver
"""

import numpy as np
from phasereg import Form, pgrad
from .lddmm_base import LDDMMBase



class LDDMM(LDDMMBase):
    def __init__(self, I0, I1, K, step_num=5, sigma=0, check_error=True, save_best=0):
        """
        Class for solving LDDMM.
        I0: 2D numpy array
        I1: 2D numpy array
        step_num: number of time steps (~5)
        K: RKHS operator (see: LDDMM)
        """

        LDDMMBase.__init__(self, I0, I1, K,
                           step_num=step_num, sigma=sigma, save_best=save_best,
                           check_error=check_error)



