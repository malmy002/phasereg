"""
Mass-preserving LDDMM solver for CDI.

"""
import numpy as np
from numpy.fft import fft2, ifft2, fftshift
from phasereg import Form, K, grad, pgrad, pgrad, div, KHelmholtz
from .lddmm_base import LDDMMBase
from phasereg.src.utils import scale_volforms


class Mass_LDDMM_CDI(LDDMMBase):
    def __init__(self, I0, I1, K, step_num=5, sigma=0, eps=0, img_bg=1e-3,
                 save_best=0, check_error=True):
        """Class for solving mass-preseving LDDMM for CDI.

        ---------------
        Images: Centered
        -----------------

        I0: 2D numpy array, template image (centered)
        I1: 2D numpy array, far-field amplitude data (centered)
        K: K operator (RHKS)
        step_num: number of time steps (~5)
        sigma: weight for regularization term (||v||)
        eps: regularization constant (in Fourier space)
        """
        # min values
        self.bg = img_bg

        LDDMMBase.__init__(self, I0, I1, K, step_num=step_num, sigma=sigma,
                           save_best=save_best,
                           check_error=check_error)

        #Re-init forms
        self._init_forms(I0,I1)

        # Type
        self.type = "mass_lddmm_cdi"

        #Regularization constant
        self.eps = eps
        self.I1norm = np.linalg.norm(I1.flat, ord=2)

        M, N = self.I0.shape

        #Saving results
        self.J_hist = []  # saving Js
        self.it_hist = []  # saving iteration numbers

        #Residual
        self.R0  = Form(0, array=np.zeros((M,N)))

        #Mask
        self.mask = np.ones((M,N), dtype='bool')
        self.mask[0,0] = 0

        self.Js0, self.Js1= [], []
        for b in range(self.step_num):
            self.Js0.append(Form(2, array=I0))
            self.Js1.append(Form(2, array=I1))

    def save_results(self):
        self.J_hist.append(self.Js0[-1])
        self.it_hist.append(self.it)


    def calculate_error(self):
        self.error.append(np.linalg.norm(self.R0.flat,2)/self.I1norm)


    def update_velocities(self, step_size):
        """ Calculate velocities
        """
        # for b in range(self.step_num):
        #     self.vs[b] -= step_size * (self.sigma * self.vs[b] + \
        #     self.K(self.Js0[b] * pgrad(self.Js0[b] - self.Js1[b])))

        for b in range(self.step_num):
            self.vs[b] -= step_size * (self.sigma * self.vs[b] + \
            self.K(self.Js10[b] * pgrad(self.Js10[b] - self.Js1[b])))

    def update_Js(self):
        """Update Js
        """
        uf = fft2(fftshift(self.Js0[-1]), norm='ortho')
        Jf = self.I1 * np.exp(1j * np.angle(uf))
        J1 = np.real(fftshift(ifft2(Jf, norm='ortho')))
        J1 = Form(2, array=J1)

        for b in range(self.step_num):
            self.Js10[b] = self.Js0[-1].pull(self.phis1[b])
            
        for b in range(self.step_num):
            self.Js0[b] = self.I0.pull(self.phis0[b])
            self.Js1[b] = J1.pull(self.phis1[b])

        self.R0 = np.abs(uf) - self.I1


    def _init_forms(self, I0, I1):
        """
        :param I0:
        :param I1:
        :return:
        """
        self.I0 = Form(2, array=I0)
        self.I1 = Form(2, array=fftshift(I1))





