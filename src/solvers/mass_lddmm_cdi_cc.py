"""

Mass-preserving LDDMM solver for CDI.

Important Notes:
*KEY: Normalization of images*
    Ratio of min-max values dictates the nature of the method.
        - If min values are large wrt to max values, then the mass can be moved in from anywhere.
        - Small min --> ~ LDDMM (transports mass)
"""
import numpy as np
from numpy.fft import fft2, ifft2, fftshift
from phasereg import Form, K, grad, pgrad, div, KHelmholtz
from .lddmm_base import LDDMMBase
from phasereg.src.utils import scale_volforms
from ..utils.cost_function import cost1


class Mass_LDDMM_CDI_CC(LDDMMBase):
    def __init__(self, I0, I1, K, step_num=5, sigma=0, eps=0, img_bg=1e-3,
                 save_best=0, check_error=True):
        """Class for solving mass-preserving LDDMM for CDI with cross-correlation.

         ---------------
        Images: Centered
        -----------------

        I0: 2D numpy array, template image (centered)
        I1: 2D numpy array, far-field amplitude data (centered)
        K: K operator (RHKS)
        step_num: number of time steps (~5)
        sigma: weight for regularization term (||v||)
        eps: regularization constant (in Fourier space)
        """
        # min values
        self.bg = img_bg

        self.M, self.N = I0.shape

        LDDMMBase.__init__(self, I0, I1, K, step_num=step_num, sigma=sigma, save_best=save_best,
                           check_error=check_error)

        #Re-init forms
        self._init_forms(I0,I1)

        # Type
        self.type = "mass_lddmm_cdi"

        #Regularization constant
        self.eps = eps
        self.I1norm = np.linalg.norm(I1.flat, ord=2)

        #Cross-correlation: A^2/(B*C)
        self.A = self.B = 1.0

        #Saving results
        # self.history = []
        self.J_hist = []  # saving Js
        self.it_hist = []  # saving iteration numbers

        #Mask
        self.mask = np.ones((self.M,self.N), dtype='bool')
        self.mask[0,0] = 0

        self.cost1, self.total_cost = [], []

        self.Js10 = []
        for b in range(self.step_num):
            self.Js10.append(Form(2, array=self.I0))
        self.Rp = Form(2, array=np.zeros(self.I0.shape))

        self.update_Js()


    def save_results(self):
        self.J_hist.append(self.Js0[-1])
        self.it_hist.append(self.it)


    def calculate_error(self):
        self.error.append(-self.A**2/(self.B * self.C))
        self.cost1.append(cost1(self.vs, self.K, self.sigma))
        self.total_cost.append(self.error[-1] + self.cost1[-1])


    def update_velocities(self, step_size):
        """ Calculate velocities
        """
        for b in range(self.step_num):
            self.vs[b] -= step_size * (
            self.sigma * self.vs[b] + \
            self.N **2 * (self.A/(self.B*self.C)) *  \
            self.K(self.Js10[b] * pgrad((self.Rp(self.phis1[b])))))


    def update_Js(self):
        """Update Js
        """
        uf = fft2(fftshift(self.Js0[-1]), norm='ortho')
        uf_av = np.mean(np.abs(uf))
        ufbar = np.abs(uf) - uf_av

        #A, B:
        self.A = np.sum((np.abs(uf) - uf_av) * (self.I1 - self.I1.mean()))
        self.B = np.sum((np.abs(uf) - uf_av)**2)

        #(A/B)*Beta'
        I0v = (self.A/self.B)*np.real(fftshift(ifft2(ufbar*np.exp(1j*np.angle(uf)),
                norm='ortho')))

        #b'
        Jf = (self.I1bar) * np.exp(1j * np.angle(uf))
        J1 = np.real(fftshift(ifft2(Jf, norm='ortho')))

        #R'
        self.Rp[:] = I0v - J1

        for b in range(self.step_num):
            self.Js10[b] = self.Js0[-1].pull(self.phis1[b])
        for b in range(self.step_num):
            self.Js0[b] = self.I0.pull(self.phis0[b])
        # self.Js0[-1] = self.I0.pull(self.phis0[-1])


    def _init_forms(self, I0, I1):
        """
        Rescale images st sigma is invariant wrt N & intensity values.

        :param I0:
        :param I1:
        :return:
        """
        self.I0 = Form(2, array=I0/I0.max())
        self.I1 = Form(2, array=fftshift(I1)/I1.max())
        self.I1bar = Form(2, array=self.I1 - np.mean(self.I1))
        self.C = np.sum(self.I1bar**2)
