"""

LDDMM solver


"""
import numpy as np
from phasereg import Form, K, grad, pgrad, KHelmholtz
from ..utils.distance_metric import distance
from matplotlib import pylab as plt
from ..utils.cost_function import cost1


class LDDMMBase(object):
    def __init__(self, I0, I1, K, step_num=5, sigma=0, save_best=True, check_error=True):
        """
        Class for solving LDDMM.
        I0: 2D numpy array, template image
        I1: 2D numpy array, target image
        K: K operator (RHKS)
        sigma: weight for regularization term (||v||)
        step_num: number of time steps (~5)
        """

        #Type
        self.type = "base"

        # Initial images
        self._init_forms(I0, I1)

        M, N = self.I0.shape

        self.it = 0

        # Number of time steps
        self.step_num = step_num

        # dt
        self.dt = 1.0 / (step_num)
        # self.dt = 1.0 / (step_num - 1.0)

        # Velocity field
        self.v = Form(1, shape=(2, M, N))

        # K operator
        self.K = K

        #Regularization weight (on ||v|| term)
        self.sigma = sigma

        #For normalization of error
        self.I1norm = np.linalg.norm(I1.flat, ord=2)

        #Check error & break if error[-1] > mean(error[-5:])
        self.check_error = check_error

        #Costs: total_cost = cost1 + error (distance + similarity)
        self.cost1, self.total_cost, self.error = [], [], []

        # Saving the best results
        self.save_best = save_best
        class BestResults: pass
        self.best_results = BestResults

        # Initialize
        self.phis0, self.phis1, self.Js0, self.Js1, self.Js10, self.vs = \
            [], [], [], [], [], []
        for b in range(self.step_num):
            self.vs.append(Form(1, array=np.zeros((2, M, N))))
            self.phis0.append(Form(1, array=self.v.x))
            self.phis1.append(Form(1, array=self.v.x))
            self.Js0.append(Form(0, array=I0))
            self.Js10.append(Form(0, array=I0))
            self.Js1.append(Form(0, array=I1))

    def _init_forms(self,I0,I1):
        """
        Set max(I0), max(I1) = 1 to ensure E1, E2 are balanced correctly.
        s.t. sigma doesn't need to be changed each time.
        """
        self.I0 = Form(0, array=I0/I0.max())
        self.I1 = Form(0, array=I1/I1.max())

    def run(self, itnum, step_size=1e-3, save=None, K=None, sigma=None):
        """
        Run LDDMM

        step_size: number or StepSize class
        sigma: regularization term weight
        """
        if K is not None:
            self.K = K

        if sigma is not None:
            self.sigma = sigma

        for a in range(itnum):

            #Update velocities
            self.update_velocities(step_size)

            #Update diffeomorphisms
            self.update_phis()

            # Update Js
            self.update_Js()

            #Calculate error
            if np.mod(a, 10) == 0:
                self.calculate_error()

                #Save results if error is minimum
                if self.save_best:
                    if self.error[-1] == np.min(self.error):
                        self.save_best_results()

            #Save
            if save:
                if np.mod(a,save)==0:
                    self.save_results()

            self.it += 1

            if (len(self.error) > 5) & self.check_error:
                if self.error[-1] > np.mean(self.error[-10:]):
                    print('Stopping iterations at: %d .' %self.it)
                    self.revert_to_best()
                    self.error.pop(-1)
                    break


    def save_best_results(self):
        """
        Save best results
        :return:
        """
        self.best_results.Js0 = self.Js0.copy()
        self.best_results.Js1 = self.Js1.copy()
        self.best_results.Js10 = self.Js10.copy()
        self.best_results.phis0 = self.phis0.copy()
        self.best_results.phis1 = self.phis1.copy()
        self.best_results.vs = self.vs.copy()

    def revert_to_best(self):
        self.Js0 = self.best_results.Js0.copy()
        self.Js1 = self.best_results.Js1.copy()
        self.Js10 = self.best_results.Js10.copy()
        self.phis0 = self.best_results.phis0.copy()
        self.phis1 = self.best_results.phis1.copy()
        self.vs = self.best_results.vs.copy()

    def save_results(self):
        pass

    def calculate_error(self):
        self.error.append(np.linalg.norm(self.Js0[-1] - self.I1, 2)/self.I1norm)
        self.cost1.append(cost1(self.vs, self.K, self.sigma))
        self.total_cost.append(self.error[-1] + self.cost1[-1])

    def update_velocities(self, step_size):
        """ Calculate velocities
        """
        if self.sigma == 0:
            for b in range(self.step_num):
                self.vs[b] = self.vs[b] + step_size * self.K(self.phis1[b].jac_det(self.phis1[b]) *
                                                 pgrad(self.Js0[b]) * (self.Js0[b] - self.Js1[b]))
        else:
            for b in range(self.step_num):
                self.vs[b] = self.vs[b] - step_size *(self.sigma * self.vs[b] -
                                                 self.K(self.phis1[b].jac_det(self.phis1[b]) *
                                                 pgrad(self.Js0[b]) * (self.Js0[b] - self.Js1[b])) )
    def update_Js(self):
        """Update Js
        """
        for b in range(self.step_num):
            self.Js0[b] = self.I0(self.phis0[b])
            self.Js1[b] = self.I1(self.phis1[b])

    def update_phis(self):
        """
        Update diffeomorphisms
        :return:
        """
        for b in range(self.step_num):
            if b != 0:
                self.phis0[-b] = self.phis0[-b - 1](self.phis0[-b].x - self.dt * self.vs[-b])
            if b + 1 != self.step_num:
                self.phis1[b] = self.phis1[b + 1](self.phis1[b].x + self.dt * self.vs[b])

    def calc_displacement(self, vt):

        disp = self.dt * vt.copy()
        for a in range(5):
            disp = self.dt * vt(vt.x - disp/2.0)
        return disp

    def show_evolution(self, cm='gray_r', figsize=(14,5), ticks=False):
        plt.figure(figsize=figsize)
        for a in range(self.step_num):
            plt.subplot(1, self.step_num, a+1)
            plt.imshow(self.Js0[a], cmap=cm)
            if ~ticks:
                plt.xticks([]); plt.yticks([])
        plt.tight_layout()

    def distance(self):
        """
        Calculate distance.
        :return: distance (scalar)
        """
        return distance(self.vs, self.K)
