"""

LDDMM solver for CDI using correlation coefficient for similarity metric.

IMPORTANT NOTE: Update order is important: vs -> phis -> Js


E_(CC) = -A^2/(BC)

"""
import numpy as np
from scipy.ndimage import gaussian_filter
from numpy.fft import fft2, ifft2, fftshift
from phasereg import Form, pgrad, grad, KHelmholtz
from .lddmm_base import LDDMMBase
from ..utils.cost_function import cost1

class LDDMM_CDI_CC(LDDMMBase):
    def __init__(self, I0, I1, K, step_num=5, sigma=0, eps=0,
                 save_best=True, check_error=True):
        """
        Class for solving LDDMM for CDI (correlation coefficient similarity).
        I0: 2D numpy array, template image (centered)
        I1: 2D numpy array, far-field amplitude data (centered)
        K: K operator (RHKS)
        step_num: number of time steps (~5)
        sigma: weight for regularization term (||v||)
        eps: regularization constant (in Fourier space)
        """

        self.M, self.N = I0.shape

        LDDMMBase.__init__(self, I0, I1, K,
                           step_num=step_num, sigma=sigma, save_best=save_best,
                           check_error=check_error)

        # Type
        self.type = "lddmm_cdi_cc"

        #Regularization constant
        self.eps = eps

        #reference number of pixels (used for calculating integrals in Fourier space)
        self.N0 = 100

        #Saving results
        self.J_hist = []  # saving Js
        self.it_hist = []  # saving iteration numbers

        #Initialize variables
        self.Js10 = []
        for b in range(self.step_num):
            self.Js10.append(Form(0, array=self.I0))
        self.Rp = Form(0, array=np.zeros(self.I0.shape))

        #Same as grad(E)=0 initially
        self.update_Js()

        self.cost1, self.total_cost = [], []


    def save_results(self):
        self.J_hist.append(self.Js0[-1])
        self.it_hist.append(self.it)


    def calculate_error(self):
        self.error.append(-self.A**2/(self.B * self.C))
        self.cost1.append(cost1(self.vs, self.K, self.sigma))
        self.total_cost.append(self.error[-1] + self.cost1[-1])


    def update_velocities(self, step_size):
        """ Calculate velocities
        """
        for b in range(self.step_num):
            self.vs[b] = self.vs[b] - step_size * (self.sigma * self.vs[b] -
                            self.N**2 * (self.A/(self.B*self.C)) *
                           self.K(self.phis1[b].jac_det(self.phis1[b]) *
                          pgrad(self.Js10[b]) * (self.Rp(self.phis1[b]))))

    def _init_forms(self,I0,I1):
        """
        Rescale images st sigma is invariant wrt N & intensity values.
        """
        self.I0 = Form(0, array=I0/I0.max())
        self.I1 = Form(0, array=fftshift(I1)/I1.max())
        self.I1bar = Form(0, array=self.I1 - np.mean(self.I1))
        self.C = np.sum(self.I1bar**2)


    def update_Js(self):
        """Update Js
        """
        uf = fft2(fftshift(self.Js0[-1]), norm='ortho')
        uf_av = np.mean(np.abs(uf))
        ufbar = np.abs(uf) - uf_av

        #A, B:
        self.A = np.sum((ufbar) * (self.I1bar))
        self.B = np.sum((ufbar)**2)

        I0v = (self.A/self.B)*np.real(fftshift(
                ifft2(ufbar * np.exp(1j*np.angle(uf)), norm='ortho')))

        Jf = (self.I1bar) * np.exp(1j * np.angle(uf))
        J1 = np.real(fftshift(ifft2(Jf, norm='ortho')))

        """phi_(r,s) o phi_(s,r) not equal: 1"""
        for b in range(self.step_num):
            self.Js10[b] = self.Js0[-1](self.phis1[b])
            self.Js0[b] = self.I0(self.phis0[b])

        # R'
        self.Rp[:] = I0v - J1
