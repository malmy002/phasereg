# Phase retrieval via non-rigid image registration
##### Author: Erik Malm
<br/>
![alt text](docs/overview.png)

## Overview:
This repository contains the source code for the article "Phase retrieval via non-rigid image registration".

The algorithms perform phase retrieval by deforming an intial image until the diffraction pattern matches the data.

The article can be found on arXiv at: https://arxiv.org/abs/2306.15701 .

## Registration algorithms:
- Large deformation diffeomorphic metric mapping (LDDMM)
    - Geometric and mass-preserving actions
- Small deformation using gradient descent

### Core dependencies:
- Numpy
- Scipy


### Plotting dependencies:
- Matplotlib







